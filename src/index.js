'use strict'
/**
*  CALE  AI for DIY Healthscience toolkit
*
*
* @class CALE
* @package    CALE AI
* @copyright  Copyright (c) 2020 James Littlejohn
* @license    http://www.gnu.org/licenses/old-licenses/gpl-3.0.html
* @version    $Id$
*/
import EventEmitter from 'events'
import Population from './population/population.js'
import Gene from './gene/gene.js'

class CALE extends EventEmitter {

  constructor() {
    super()
    this.liveGene = new Gene()
    this.livePopulation = {}
  }

  /**
  *
  * @method askCALE
  *
  */
  askCALE = function () {
    let outFlow = {}
    outFlow.type = 'hop-learn'
    outFlow.action = 'cale-evolution'
    outFlow.task = 'begin'
    outFlow.data = { name: 'cale-evolution', status: 'loaded'}
    this.emit('cale-evolution', outFlow)
  }

  /**
  *
  * @method stageListeners
  *
  */
  stageListeners = function () {
    this.livePopulation.on('population-stage', async (data) => {
      let CaleUpdate = {}
      CaleUpdate.stage = 'population'
      CaleUpdate.hop = { nxp: 'ce-1233221' }
      CaleUpdate.model = { model: 'ce-1233221' }
      CaleUpdate.data = data
      this.emit(CaleUpdate)
    })

    this.livePopulation.on('model-stage', async (data) => {
      let CaleUpdate = {}
      CaleUpdate.stage = 'model'
      CaleUpdate.hop = { nxp: 'ce-1233221' }
      CaleUpdate.model = { model: 'ce-1233221' }
      CaleUpdate.data = data
      this.livePopulation.emit(CaleUpdate)
    })

    this.livePopulation.on('score-stage', async (data) => {
      let CaleUpdate = {}
      CaleUpdate.stage = 'score'
      CaleUpdate.hop = { nxp: 'ce-1233221' }
      CaleUpdate.model = { model: 'ce-1233221' }
      CaleUpdate.data = data
      this.emit(CaleUpdate)
    })

    this.livePopulation.on('cycles-stage', async (data) => {
      let CaleUpdate = {}
      CaleUpdate.stage = 'cycles'
      CaleUpdate.hop = { nxp: 'ce-1233221' }
      CaleUpdate.model = { model: 'ce-1233221' }
      CaleUpdate.data = data
      this.emit('cale-evolution', CaleUpdate)
    })

    this.livePopulation.on('complete-stage', async (context, data) => {
      // last model best
      let countLast = Object.keys(data)
      let lastModel = countLast.pop()
      let bestModel = data[lastModel]
      let CaleUpdate = {}
      CaleUpdate.type = context.type
      CaleUpdate.action = context.action
      CaleUpdate.task = context.task
      CaleUpdate.context = context
      CaleUpdate.stage = 'complete'
      CaleUpdate.hop = { nxp: 'ce-1233221' }
      CaleUpdate.model = { model: bestModel }
      CaleUpdate.data = data
      this.emit('cale-evolution', CaleUpdate)
    })

  }

  /**
  *
  * @method CALEflow
  *
  */
  CALEflow = function (model) {
    // set model up as genes
    this.liveGene.setGenes(model)
    this.livePopulation = new Population(model, this.liveGene)
    this.stageListeners()
    this.livePopulation.startPopulation({})
  }

  /**
  *
  * @method AIlisteners
  *
  */
  AIlisteners = function () {
    
  }

  /**
  *
  * @method populationManager
  *
  */
  populationManager = function (data) {
    this.livePopulation.createPopulation(data)
  }

}

export default CALE
