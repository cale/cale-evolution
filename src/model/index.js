'use strict'
/**
*  model
*
*
* @class Model
* @package    CALE AI
* @copyright  Copyright (c) 2020 James Littlejohn
* @license    http://www.gnu.org/licenses/old-licenses/gpl-3.0.html
* @version    $Id$
*/
import util from 'util'
import EventEmitter from 'events'

class Model extends EventEmitter {
  constructor() {
    super()

  }

  /**
  *
  * @method baseEquation
  *
  */
  baseEquation = function () {
    console.log('base model autoregression')
    console.log(' prediction = constant + b (D t-1)')
    let baseConstant = 60 // constant variable
    let coeff1 = 0.8  // model variable
    let timeMinus1 = 72 // data value yesterday (avg. heart rate for day)
    let coeff2 = 0.2  // model variable
    let timeMinus2 = 68 // data value two days ago (avg. heart rate for day)
    let pFuture = baseConstant + (coeff1 * timeMinus1) + (coeff2 * timeMinus2)
    let modelGene = [baseConstant, coeff1, coeff2]
    return modelGene
  }

}
export default Model
