'use strict'
/**
*  Population
*
*
* @class Population
* @package    Population AI
* @copyright  Copyright (c) 2020 James Littlejohn
* @license    http://www.gnu.org/licenses/old-licenses/gpl-3.0.html
* @version    $Id$
*/
import util from 'util'
import EventEmitter from 'events'

class Population extends EventEmitter {

  constructor(model, gene) {
    super()
    this.context = model
    this.geneLive = gene
    this.genesisPopulation = []
    this.populationSize = 100
    this.generationcycles = 180
    this.currentDay = []
  }

  /**
  *
  * @method startPopulation
  *
  */
  startPopulation = function () {
    let groundModel = this.geneLive.baseModel.baseEquation()
    this.genesisPopulation.push({ id: 'live', data: [60, 70, 89, 75, 67], model: groundModel })
    // how many day profiles to create?
    let populationLeft = this.populationSize - this.genesisPopulation.length
    // use default population size
    let randPopulation = this.randPopulation(populationLeft, groundModel)
    // console.log('inital starting population of genes')
    let firstPopulation = [...this.genesisPopulation, ...randPopulation]
    // score current or none trained data
    let startData =  this.syntheticData()
    let generationcycles = [...Array(10).keys()].map(generationcycles => generationcycles + 1)  // [1, 2, 3, 4, 5]
    let bestModel = {}
    for (let cycle of generationcycles) {
      if (cycle === 1) {
        bestModel = this.geneLive.newGeneration(cycle, firstPopulation, startData)
      } else {
        // add new data to peers time series
        let newData = this.addSynthetic()
        let fitModel = this.geneLive.newGeneration(cycle, bestModel.population, newData)
        // console.log('firrest model')
        // console.log(fitModel.model)
      }
      // this.makeFuture()
    }
    console.log('CALE finished learning')
    // console.log(this.geneLive.generationHolder)
    // console.log(util.inspect(this.geneLive.generationHolder, {showHidden: false, depth: null, colors: true}))
    this.emit('complete-stage', this.context, this.geneLive.generationHolder)
  }

  /**
  * form knowledge bundle input for safeFLOW-ECS
  * @method makeFuture
  *
  */
  makeFuture = function (model) {
    let message = {}
    message.type = 'safeflow'
    message.reftype = 'ignore'
    message.action = 'predictionexperiment'
    message.data = {} // ECSbundle
    const safeFlowMessage = JSON.stringify(message)
    this.emit('newFuture', safeFlowMessage)
  }

  /**
  *
  * @method randPopulation
  *
  */
  randPopulation = function (size, groundModel) {
    // just create synthetic data for now (average heart rate of life, yesterday average, two days ago avg. etc.)
    let newPopulation = []
    let num = size
    while(num >=1) {
      let coeff1 = groundModel[1] * Math.random().toFixed(1)
      let coeff2 = groundModel[2] * Math.random().toFixed(1)
      newPopulation.push({ id: num, model: [groundModel[0], coeff1.toFixed(1), coeff2.toFixed(1)] })
      num--
    }
    return newPopulation
  }

  /**
  *
  * @method syntheticData 
  *
  */
  syntheticData = function (cycle) {
    const baseConst = 60
    let today = 88
    let yesterdayData = 77
    let twoDaysago = 69
    let threeDaysago = 83
    // use random movement within boundry 0 to 1
    let num = cycle
    let diff1 = 0.01
    let diff2 = -0.01
    let timeseriesData = [today, yesterdayData, twoDaysago, threeDaysago]
    let peerData = {}
    peerData.data = timeseriesData
    this.currentDay = peerData
    return peerData
  }

  /**
  *
  * @method addSynthetic 
  *
  */
  addSynthetic = function () {
    let dataArr = this.currentDay.data
      // which part of gene to change and by how much.
    function generateRandomNumber(min, max) {
      return Math.floor(Math.random() * (max - min + 1) + min)
    }
    // Generate a random number between 1 and 10 (including 1 and 10)
    let randomNum = generateRandomNumber(60, 78)
    let newDataElement = randomNum
    dataArr.unshift(newDataElement)
    let updatedData = {}
    updatedData.data = dataArr
    return updatedData
  }

  /**
  *
  * @method createPopulation
  *
  */
  createPopulation = function (data) {
  }

}

export default Population
