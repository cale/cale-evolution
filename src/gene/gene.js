'use strict'
/**
*  Gene
*
*
* @class Gene
* @package    Gene AI
* @copyright  Copyright (c) 2020 James Littlejohn
* @license    http://www.gnu.org/licenses/old-licenses/gpl-3.0.html
* @version    $Id$
*/
import util from 'util'
import EventEmitter from 'events'
import BaseModel from '../model/index.js'

class Gene extends EventEmitter {

  constructor() {
    super()
    this.baseModel = new BaseModel()
    this.populationStart = []
    this.generationHolder = {}
    this.currentCycle = 0
  }

  /**
  *
  * @method setGenes
  *
  */
  setGenes = function (model) {
    // TODO convert model to gene format
    this.gene = this.convertModel(model)
  }

  /**
  *
  * @method convertModel
  *
  */
  convertModel = function (model) {
    console.log(model)
  }

  /**
  *
  * @method createGeneration
  *
  */
  newGeneration = function (cycle, population, live) {
    this.populationStart = population
    this.currentCycle = cycle
    this.generationHolder[cycle] = {}
    // first score the fitness of the existing population
    let fittestModel = this.scoreFitness(population, live)
    this.generationHolder[cycle] = { model: fittestModel.model, score: fittestModel.score, data: live.data }
    return fittestModel
  }

  /**
  *
  * @method scoreFitness
  *
  */
  scoreFitness = function (pop, live) {
    // first score the fitness of the existing population
    // sum of difference from recorded value to predicted value
    let sumDifference = pop.map(e => this.calcDiff(e, live) )
    let rankOrder = this.rankOrder(sumDifference)
    // match population id to model array  // pass over to crossover and mution 'breeding'
    let expandedGeneRank = []
    let bestModelGen = {}
    for (let cros of rankOrder) {
      for(let popi of pop) {
        // single out best model
        if (popi.id === rankOrder[0].id) {
          bestModelGen = popi
        }
        if (popi.id === cros.id) {
          expandedGeneRank.push(popi)
        }
      }
    }
    let corssPopulation = this.crossOver(expandedGeneRank)
    // best model make predictions
    let afterGeneCycle = {}
    afterGeneCycle.model = bestModelGen
    afterGeneCycle.score = rankOrder[0]
    afterGeneCycle.population = corssPopulation  
    return afterGeneCycle
  }

  /**
  *
  * @method calcDiff
  *
  */
  calcDiff = function (pi, live) {
    // console.log('live data')
    // console.log(live)
    let modelPredict = pi.model[0] + (live.data[1] * pi.model[1]) + (live.data[2] * pi.model[2])
    const diffModel = modelPredict - live.data[0] // what value gene model predicts - actual value
    // const diffSum = pi.data.reduce((accumulator, val, i) => accumulator + (val - live.data[i])**2, 0)
    let diffHolder = {}
    diffHolder = { id: pi.id, data: diffModel.toFixed(1) }
    return diffHolder
  }

  /**
  *
  * @method rankOrder
  *
  */
  rankOrder = function (scorePop) {
    let newOrder = []
    // first square values to get rid of negatives
    let popSquare = []
    for (let popi of scorePop) {
      let popSq = Math.pow(popi.data, 2).toFixed(1)
      popi.data = popSq
      popSquare.push(popi)
    }
    // nearest to zero
    // const goal = 0
    // const newOrder = scorePop.reduce((prev, curr) => Math.abs(curr.data - goal) < Math.abs(prev.data - goal) ? curr : prev)
    // lowest absolute value
    newOrder = popSquare.sort((a, b) => a.data - b.data)
    // return data to origal value? TODO
    return newOrder
  }

  /**
  *
  * @method crossOver
  *
  */
  crossOver = function (popRank) {
    // keep top half and random sample for real world data to make up population size
    const n = 50 //tweak this to add more items per line
    const result = new Array(Math.ceil(popRank.length / n))
      .fill()
      .map(_ => popRank.splice(0, n))
    const keepPop = result[0]
    // cross over rule?  Switch co-effients value or how many? What problem try to solve
    const mutedPop = this.mutation(keepPop)
    let newPoplutationGenes = [...keepPop, ...mutedPop]
    return newPoplutationGenes
  }

  /**
  *
  * @method mutation
  *
  */
  mutation = function (fitPop) {
    // console.log(fitPop)
    // loop through and mutate 10% randomly chose model variable to keep or change(guess -=10%)
    let randItems = this.randListGenerator(fitPop)
    let mutatedPopuation = fitPop
    return mutatedPopuation
  }

  /**
  *
  * @method randListGenerator
  *
  */
  randListGenerator = function (fitPopulation) {
    let geneUp = 0.1
    let geneDown = 0.1
    // which population gene item and what part of gene ie mode in this case?
    let mutPopulation = []
    for (let fpop of fitPopulation) {
      // which part of gene to change and by how much.
      function generateRandomNumber(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min)
      }
      // Generate a random number between 1 and 10 (including 1 and 10)
      let randomNum = generateRandomNumber(1, (fpop.model.length - 1))
      let randUpDown = generateRandomNumber(1, 2)
      let genePlace = randomNum
      let mutAmount = 0
      if (randUpDown === 1) {
        let newValue = fpop.model[genePlace] - geneDown
        mutAmount = newValue.toFixed(1)
      } else {
        let newValue = parseInt(fpop.model[genePlace]) + geneUp
        if (newValue > 0) {
          mutAmount = newValue.toFixed(1)
        } else {
          mutAmount = 0
        }
      }
      fpop.model[genePlace] = mutAmount
      let mutGene = {}
      mutGene.id =fpop.id
      mutGene.model = fpop.model
      mutPopulation.push(mutGene)
    }
    return mutPopulation
  }

}

export default Gene
